import React, { Component } from 'react';
import Grid from './components/Grid';
import ComplexGrid from './components/ComplexGrid';
import NestedGrid from './components/NestedGrid';
import Buttons from './components/Buttons';
import TabPanel from './components/TabPanel';
import Drawer from './components/Drawer';
class App extends Component{
    render(){
        return(
            // <Grid />
            // <ComplexGrid/>
            // <NestedGrid />
            // <Buttons />
            // <TabPanel />
            <Drawer />
        );
    }
}
export default App